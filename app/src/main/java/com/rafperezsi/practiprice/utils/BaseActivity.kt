package com.rafperezsi.practiprice.utils

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Looper
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.snackbar.Snackbar
import com.rafperezsi.practiprice.DashboardActivity
import com.rafperezsi.practiprice.R
import com.rafperezsi.practiprice.article.ArticleActivity


open class BaseActivity : AppCompatActivity(), DottysLocationChangeDelegates {
    //val SERVICO_DETALHES_REQUEST = 1
    var sharedPreferences: SharedPreferences? = null
    val screenWidth = Resources.getSystem().displayMetrics.widthPixels
    val screenHeigth = Resources.getSystem().displayMetrics.heightPixels
    var topOvalLayer: ConstraintLayout? = null
    var currentLocation: Location? = null
    var imageUri: Uri? = null
    var gpsTracker: GpsTracker? = null

    override fun onResume() {
        super.onResume()
        backButton()
        BaseLoader(this).hideLoader()
        if (this is ArticleActivity) {
            val bagCounterTV = (findViewById(R.id.articles_bag_counter_tv) as? TextView)
            val bagButton = (findViewById(R.id.articles_on_bag_button) as? ImageButton)
            bagCounterTV?.alpha = 0f
            bagButton?.alpha = 0f
            (findViewById(R.id.price_global_TV) as? TextView)?.alpha = 0f
            bagButton?.isEnabled = false
            bagCounterTV?.isEnabled = false
        }

        GpsTracker(this).getLocation()


//        topOvalLayer = findViewById<ConstraintLayout>(R.id.top_oval_layer)
//        topOvalLayer?.setOvalTopLayout(this)
    }

    override fun onStart() {
        super.onStart()
        gpsTracker = GpsTracker(this)
        gpsTracker?.locationObserver = DottysLocationObserver(this)
    }

    fun hideKeyboard() {
        try {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(window.decorView.rootView.windowToken, 0)
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        } catch (e:Exception){}
    }


    fun showMessage(msg: String) {
        hideKeyboard()
        val container = this.findViewById<View>(android.R.id.content)
        if (container != null) {
            Snackbar.make(container, msg, Snackbar.LENGTH_LONG).show()
        }
    }

    fun backButton() {
        val welcomeTextView = findViewById(R.id.welcome_textview) as? TextView
//        val backButton = findViewById(R.id.back_arrow_button) as? ImageButton
//        backButton?.x = 20f//(screenWidth).toFloat()
//        backButton?.y = 10f
//        backButton?.visibility = if (this is LoginActivity) {View.GONE}else{View.VISIBLE}
//        backButton?.setOnClickListener {
//                this.finish()
//            }
        welcomeTextView?.text = setToolBarTitle()
    }

    private fun setToolBarTitle(): String {
        return when (this) {
//            is LoginActivity -> {
//               if((this as LoginActivity).loginViewModel?.isLoginView == true) {
//                   "Sign in"
//               } else {
//                   "Welcome"
//               }
//            }
            is DashboardActivity -> {
                "Últimos Artículos"
            }
            is ArticleActivity -> {
                "Agregar Artículo"
            }
            else -> {
                "🤬"
            }
        }

    }

    override fun onLocationChangeHandler(locationGps: Location?) {

        if (locationGps != null) {
            currentLocation = locationGps
        }

    }
}

class BaseLoader(val context: BaseActivity) {
    //private var progressBar: ProgressBar? = null
    //private val animations: List<String> = listOf("loader.json","loader_blue.json")
    private var progressBar: ConstraintLayout? = null
    private var animProgressBar: LottieAnimationView? = null
    fun showLoader() {
        context.hideKeyboard()
        if (progressBar?.visibility == View.VISIBLE) {
            return
        }
        progressBar = context.findViewById(R.id.progress_layout)
        animProgressBar = context.findViewById(R.id.progress_animation)
        animProgressBar?.setAnimation("loader_blue.json")
        animProgressBar?.speed = 1.5f
        animProgressBar?.playAnimation()
        animProgressBar?.loop(true)
        progressBar?.visibility = View.VISIBLE
        context.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )

    }

    fun hideLoader() {
        android.os.Handler(Looper.getMainLooper()).post {
            progressBar = context.findViewById<ConstraintLayout>(R.id.progress_layout)
            progressBar?.visibility = View.GONE
            context.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

            progressBar = null
            animProgressBar = null
        }
    }
}

enum class PreferenceTypeKey {
    USER_DATA, PREFS_DATA, PROFILE_PICTURE, ARTICLES, BAG
}

fun BaseActivity.requestCameraPermission(context: AppCompatActivity, permissionCode: Int): Boolean {
    //if system os is Marshmallow or Above, we need to request runtime permission
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (context.checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED
        ) {
            //permission was not enabled
            val permission = arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            //show popup to request permission
            context.requestPermissions(permission, permissionCode)
            return false
        } else {
            //permission already granted
            return true
        }
    } else {
        //system os is < marshmallow
        return true
    }
}