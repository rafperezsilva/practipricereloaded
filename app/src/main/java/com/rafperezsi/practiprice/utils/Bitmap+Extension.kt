package com.rafperezsi.practiprice.utils

import android.content.res.Resources
import android.graphics.Bitmap
import android.os.Build
import android.util.Base64
import android.util.Log
import androidx.core.graphics.scale
import java.io.ByteArrayOutputStream
import java.lang.Exception
import kotlin.math.roundToInt

fun Bitmap.encodeToString(): String? {
    return try {
        val byteArrayOutputStream = ByteArrayOutputStream()
        val whidth = this.getScaledWidth(Resources.getSystem().displayMetrics)
        val heigth = this.getScaledHeight(Resources.getSystem().displayMetrics)
        val temBM = this.scale((whidth*0.7).roundToInt(),(heigth*0.7).roundToInt(),true)

        temBM.compress(Bitmap.CompressFormat.JPEG, 55, byteArrayOutputStream)
        Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
    }catch (e: Exception){
        Log.e("BITMAP ENCODE","${e.message}")

        null
    }
}

fun Bitmap.getBitmapSize():Int{
    return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
        this.rowBytes * this.height
    } else this.byteCount
}