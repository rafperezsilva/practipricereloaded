package com.rafperezsi.practiprice.utils.extensions
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.rafperezsi.practiprice.R
import com.rafperezsi.practiprice.utils.BaseActivity

import kotlin.math.roundToInt

fun ConstraintLayout.setOvalTopLayout(context: BaseActivity){
//
    val includeLayout  = context.findViewById(R.id.top_layer_layout_include) as? ConstraintLayout
    val mainWidthSize = (context.screenWidth*0.7).toFloat()
    val layerSize = context.screenWidth+context.screenWidth*0.35
    val ovalParams = ConstraintLayout.LayoutParams(layerSize.roundToInt(), context.screenWidth)
    this.x = -((layerSize-context.screenWidth)/2).toFloat()
    includeLayout?.y = -mainWidthSize
    this.layoutParams = ovalParams
    val topLayerHeigt = (includeLayout?.y?.plus(context.screenWidth)) ?: 0f
    val mainHeightSize = (context.screenHeigth*0.98 - topLayerHeigt)
    val mainLayout = context.findViewById(R.id.main_layout) as? ConstraintLayout
    mainLayout?.let{
        it.layoutParams = ConstraintLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                mainHeightSize.roundToInt())
        it.y = (topLayerHeigt*0.9).toFloat()
    }
}
