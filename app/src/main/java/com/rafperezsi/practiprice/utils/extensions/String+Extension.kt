package com.bluecoltan.dottys.utils.extensions

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.os.Build
import android.text.*
import android.text.style.AbsoluteSizeSpan
import android.text.style.AlignmentSpan
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.Log
import android.util.Patterns
import com.rafperezsi.practiprice.utils.BaseActivity
import org.skyscreamer.jsonassert.JSONAssert
import java.math.BigInteger
import java.math.RoundingMode
import java.security.MessageDigest
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.regex.Pattern

fun String.isEquivalentToString(jsonString:String?):Boolean{
    return try {
        JSONAssert.assertEquals(this, jsonString, false)
        true
    } catch (e: Error) {
        false
    }
}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@SuppressLint("SimpleDateFormat")
fun String.stringToDate(): Date {
    return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").parse(this.replace("Z", ""))
}
fun String.stringGetYear(): Int {
    val date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").parse(this.replace("Z", ""))
    val cal = Calendar.getInstance(TimeZone.getTimeZone("America/Los_Angeles"))

    cal.time = date
    return cal.get(Calendar.YEAR)

}
fun String.maskPhoneNumber():String {
    return try {
        "(xxx)xxx-${this.substring(this.length - 4, this.length)}"
    } catch (e: Exception){
        Log.e("MASK PHONE","${e.message}")
        ""
    }
}
fun String.getleftDays(): String {
    val date: Date =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").parse(this.replace("Z", ""))
    val diferenceAtTime = date.time - Date().time
    return "end in " + (diferenceAtTime / (1000 * 3600 * 24)).toString() + " days"
}

fun String.encodeToBitmap():Bitmap? {
    return try {
        val decodedByte = Base64.decode(this, Base64.DEFAULT)
        BitmapFactory.decodeByteArray(decodedByte, 0,decodedByte?.size ?: return null)
    }catch (e: java.lang.Exception){
        Log.e("BITMAP DECODE","${e.message}")
        null
    }
}

fun String.isValidPassword(context: BaseActivity): Boolean {
    val str = this
    var valid = true
    var mssg = String()
    // Password policy check
    // Password should be minimum minimum 8 characters long
    if (str.length < 7) {
        mssg = "Must be have at least 6 characters"
        valid = false
    }
    // Password should contain at least one number
//        var exp = ".*[0-9].*"
//        var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
//        var matcher = pattern.matcher(str)
//        if (!matcher.matches()) {
//            valid = false
//        }

    // Password should contain at least one capital letter
    var exp = ".*[A-Z].*"
    var pattern = Pattern.compile(exp)
    var matcher = pattern.matcher(str)
    if (!matcher.matches()) {
        mssg = "Must be have at least one capital letter"
        valid = false
    }

    // Password should contain at least one small letter
    exp = ".*[a-z].*"
    pattern = Pattern.compile(exp)
    matcher = pattern.matcher(str)
    if (!matcher.matches()) {
        mssg = "Must be have at least one small letter"
        valid = false
    }

    // Password should contain at least one special character
    // Allowed special characters : "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
//        exp = ".*[~!@#\$%\\^&*()\\-_=+\\|\\[{\\]};:'\",<.>/?].*"
//        pattern = Pattern.compile(exp)
//        matcher = pattern.matcher(str)
//        if (!matcher.matches()) {
//            valid = false
//        }

    // Set error if required
//        if (updateUI) {
//            val error: String? = if (valid) null else PASSWORD_POLICY
//            setError(data, error)
//        }
    if (!valid) {
        context.showMessage(mssg)
    }
    return valid
}

fun String.isValidEmail(): Boolean {
    return if (TextUtils.isEmpty(this)) {
        false
    } else {
        Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }
}


fun String.currentDateTime():String{
    val current = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
        LocalDateTime.now()
    } else {
        return  ""
    }
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
    return  current.format(formatter)

}

   fun isProbablyAnEmulator() = Build.FINGERPRINT.startsWith("generic")
        || Build.FINGERPRINT.startsWith("unknown")
        || Build.MODEL.contains("google_sdk")
        || Build.MODEL.contains("Emulator")
        || Build.MODEL.contains("Android SDK built for x86")
        || Build.BOARD == "QC_Reference_Phone" //bluestacks
        || Build.MANUFACTURER.contains("Genymotion")
        || Build.HOST.startsWith("Build") //MSI App Player
        || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
        || "google_sdk" == Build.PRODUCT


/*
* BITMAP EXTENSION
* */

fun Bitmap.rotateBitmap():Bitmap{
    val matrix = Matrix()

    matrix.postRotate(90F)

    val scaledBitmap = Bitmap.createScaledBitmap(this, width, height, true)

    return  Bitmap.createBitmap(scaledBitmap,
        0,
        0,
        scaledBitmap.width,
        scaledBitmap.height,
        matrix,
        true)
}
fun Bitmap.rotateCustomBitmap(degreess:Float):Bitmap {
    val matrix = Matrix()

    matrix.postRotate(degreess)

    val scaledBitmap = Bitmap.createScaledBitmap(this, width, height, true)

    return Bitmap.createBitmap(
        scaledBitmap,
        0,
        0,
        scaledBitmap.width,
        scaledBitmap.height,
        matrix,
        true
    )
}

 fun attributtedSimple(context: BaseActivity, firtsText: String, secondText: String): SpannableString {
    val spannable = SpannableString("$firtsText $secondText")
    spannable.setSpan(
        ForegroundColorSpan(Color.BLACK),
        0, firtsText.length,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    spannable.setSpan(
        AbsoluteSizeSpan(22, true), 0, firtsText.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    spannable.setSpan(
        AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), firtsText.length, spannable.length,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    spannable.setSpan(
        AbsoluteSizeSpan(35, true), 0, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return spannable
}


fun Double.roundOffDecimal(): Float? {
    val df = DecimalFormat("#.#")
    df.roundingMode = RoundingMode.HALF_DOWN
    try {
        return df.format(this).toFloat()
    } catch (e: java.lang.Exception) {
        Log.e("ON CAT FLOAT", "${e.message}")
    }
    return  null
}