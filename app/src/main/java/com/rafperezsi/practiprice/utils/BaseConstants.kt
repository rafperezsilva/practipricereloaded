package com.rafperezsi.practiprice.utils

object BaseConstants  {
    enum class IntentKeys  {
        IS_LOGIN_VIEW
    }
    const val PERMISSION_CROP_IMAGE = 1007
    const val PERMISSION_CODE_CAMERA = 1000
    const val PERMISSION_CAPTURE_CAMERA = 1002
    const val IMAGE_CAPTURE_CODE = 1001
    const val SERVICO_DETALHES_REQUEST = 1
    const val RECORD_AUDIO_REQUEST_CODE = 11003
    const val PICK_IMAGE_FROM_GALERY = 10005


}