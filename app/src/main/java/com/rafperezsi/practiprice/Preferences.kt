package com.rafperezsi.practiprice

import android.content.Context
import android.graphics.Bitmap
import com.rafperezsi.practiprice.article.ArticleListModel

import com.rafperezsi.practiprice.utils.BaseActivity
import com.rafperezsi.practiprice.utils.PreferenceTypeKey

object Preferences {
   /* fun Preferences.isUserLogin(context: BaseActivity): Boolean {
        return !getCurrentToken(context).isNullOrEmpty()
    }*/

    /*fun Preferences.recentRegistered(context: BaseActivity): Boolean {
            return !getCurrentUser(context)?.email.isNullOrEmpty()
        }*/

    fun saveDataPreference(
        context: BaseActivity,
        keyPreference: PreferenceTypeKey,
        jsonData: String
    ) {
        context.sharedPreferences = context.getSharedPreferences(
            PreferenceTypeKey.PREFS_DATA.name,
            0
        )
        val editor = context.sharedPreferences?.edit()
        editor?.putString(keyPreference.name, jsonData)
        editor?.apply()
    }

    /**
     * GET ARTICLES
     * */
    fun getArticlesStored(context: BaseActivity): ArticleListModel? {
        context.sharedPreferences = context.getSharedPreferences(PreferenceTypeKey.PREFS_DATA.name,
            Context.MODE_PRIVATE)
        val dataJsonAsString =
            context.sharedPreferences!!.getString(PreferenceTypeKey.ARTICLES.name, "")
        return try {
            var user: ArticleListModel? =
                dataJsonAsString?.let { ArticleListModel.fromJson(it) }
            user
        } catch (e: Exception) {
            println(e)
            null
        }
    }

   /**
     * GET BAG
     * */
    fun getBagArticlesStored(context: BaseActivity): ArticleListModel? {
        context.sharedPreferences = context.getSharedPreferences(PreferenceTypeKey.PREFS_DATA.name,
            Context.MODE_PRIVATE)
        val dataJsonAsString =
            context.sharedPreferences!!.getString(PreferenceTypeKey.BAG.name, "")
        return try {
            var user: ArticleListModel? =
                dataJsonAsString?.let { ArticleListModel.fromJson(it) }
            user
        } catch (e: Exception) {
            println(e)
            null
        }
    }


    /**
     * REMOVE PREFERENCE DATA
     */
    fun removeReferenceData(context: BaseActivity, keyPreference: PreferenceTypeKey) {
        val editor = context.sharedPreferences!!.edit()
        editor.remove(keyPreference.name)
        editor.clear()
        editor.commit()
        editor.apply()
    }

}
