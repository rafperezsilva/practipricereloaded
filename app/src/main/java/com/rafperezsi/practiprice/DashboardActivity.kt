package com.rafperezsi.practiprice

import android.content.Intent
 import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
 import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bluecoltan.dottys.utils.extensions.encodeToBitmap
import com.bluecoltan.dottys.utils.extensions.roundOffDecimal
import com.rafperezsi.practiprice.article.*
import com.rafperezsi.practiprice.utils.BaseActivity
import com.rafperezsi.practiprice.utils.BaseConstants
import com.rafperezsi.practiprice.utils.PreferenceTypeKey
import com.rafperezsi.practiprice.utils.encodeToString
import java.lang.Exception
import kotlin.math.roundToInt

class DashboardActivity : BaseActivity() {

    var dashboardViewModel: DashboardViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        dashboardViewModel = DashboardViewModel(this)

        }

    override fun onResume() {
        super.onResume()
        dashboardViewModel?.dashboardButtonsSettings()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //TODO PUT BACK
        //data?.extras?.get("data") as Bitmap
        when (requestCode) {
            BaseConstants.SERVICO_DETALHES_REQUEST -> {
                try {
                    val idCode = data?.getLongExtra(BaseConstants.IntentKeys.IS_LOGIN_VIEW.name, 0)
                    //showMessage("$idCode")
                    dashboardViewModel?.isShoppingBagActive = false
                    dashboardViewModel?.initMainArticles()
                    dashboardViewModel?.getArticleFromCode(idCode)

                } catch (e: Exception){
                    Log.e("ID CODE", "${e.message}")
                }
            }

            //}
            //TODO IMAGE HAS TAKEN AND STORED
//            val immage = findViewById<ImageView>(R.id.temporal_image)
//            immage.visibility = View.VISIBLE
//            immage.setImageBitmap(Preferences.getPictureProfileStored(this) ?: return)
//            Handler().postDelayed({
//                validationCodeViewModel?.gotoDashboard()
//            }, 5000)

            // imageView.setImageBitmap(imageBitmap)
        }

    }





    /**/

}

 class ArticlesAdapter(private val articles: List<ArticleItem>, private var context: BaseActivity) :
    RecyclerView.Adapter<ArticlesAdapter.MyViewHolder>() {
    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var mainView: ConstraintLayout = view.findViewById(R.id.item_article_cardview)
        var nameArticle: TextView = view.findViewById(R.id.name_article)
        var descriptionArticle: TextView = view.findViewById(R.id.description_article)
        var imageArticle: ImageView = view.findViewById(R.id.article_image_view)
        var codeArticle: TextView = view.findViewById(R.id.code_article)
        var priceArticle: TextView = view.findViewById(R.id.price_article_tv)
        var addArticleToBag: ImageButton = view.findViewById(R.id.add_article_to_bag)
        var seeAtTV: TextView = view.findViewById(R.id.see_at_article_TV)
        var qttyTV: TextView = view.findViewById(R.id.quantity_tv)
        var seeAtTVContainer: LinearLayout = view.findViewById(R.id.see_at_layout)
    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_article_layout, parent, false)

        return MyViewHolder(itemView)
    }

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val article = articles[position]
        article.imageItem?.let { holder.imageArticle.setImageBitmap(it.encodeToBitmap()) }
        holder.nameArticle.text = article.nameItem?.capitalize()
        holder.descriptionArticle.text = article.descriptionItem?.capitalize()
        holder.codeArticle.text = article.codeItem
        holder.seeAtTVContainer.visibility = if(article.located.isNullOrEmpty()){View.GONE}else{View.GONE}
        holder.qttyTV.visibility = if(article.quantity ?: 0 > 1){View.VISIBLE}else{View.GONE}
        holder.seeAtTV.text = article.located
        holder.qttyTV.text = "X${article.quantity ?: 0}"
        holder.priceArticle.alpha = 1f
        holder.addArticleToBag.alpha = 1f
        val dashboardActivity = (context as? DashboardActivity)
        holder.mainView.setBackgroundResource(if(dashboardActivity?.dashboardViewModel?.isShoppingBagActive == true){R.drawable.rounded_background_shop}else{R.drawable.rounded_background_list})
        holder.qttyTV.alpha = (if(dashboardActivity?.dashboardViewModel?.isShoppingBagActive == true){1.0f}else{0f})
        holder.priceArticle.text = "$${article.priceDoubleItem?.roundOffDecimal().toString()}"
        holder.addArticleToBag.setImageResource(if(dashboardActivity?.dashboardViewModel?.isShoppingBagActive == true){R.drawable.ic_baseline_remove_circle_outline_24}else{R.drawable.ic_baseline_add_shopping_cart_24})
        holder.addArticleToBag.setOnClickListener {
            var bagContent =  Preferences.getBagArticlesStored(context)?.articles ?: ArrayList<ArticleItem>()
            val animation = AnimationUtils.loadAnimation(context,R.anim.bounce_in)
            holder.addArticleToBag.startAnimation(animation)
            if(dashboardActivity?.dashboardViewModel?.isShoppingBagActive == true){
                bagContent.removeAt(position)
                if(bagContent.size == 0)
                dashboardActivity.dashboardViewModel?.isShoppingBagActive = false
                dashboardActivity.dashboardViewModel?.initMainArticles()

//                if(dashboardActivity?.articleMap?.containsKey(article.idItem)){
//                    val arrayItems =   dashboardActivity?.articleMap[article.idItem]
//                     if(arrayItems?.size ?: 0 > 0){
//                         arrayItems?.removeLast()
//                         dashboardActivity?.articleMap?.set(article.idItem, arrayItems)
//                     } else {
//                         dashboardActivity?.articleMap?.remove(article.idItem)
//                     }
//                }
            }else{

//                if(dashboardActivity?.articleMap?.containsKey(article.idItem) == true){
//                  val arrayItems =   dashboardActivity?.articleMap[article.idItem]
//                    arrayItems?.add(article)
//
//                    dashboardActivity?.articleMap?.set(article.idItem, arrayItems)
//                } else {
//                    dashboardActivity?.articleMap?.set(article.idItem, arrayListOf(article))
//                }
                bagContent.add(article)
            }

            Preferences.saveDataPreference(context,
            PreferenceTypeKey.BAG,ArticleListModel(bagContent).toJson())
            dashboardActivity?.dashboardViewModel?.updateBagButton()
        }



    }

     @ExperimentalStdlibApi
     /*fun counterBaggingMaped() :ArrayList<ArticleItem?> {
         val articles = Preferences.getBagArticlesStored(context)?.articles ?: return
         val listCathed = ArrayList<ArticleItem?>()
         for (mapped in articles) {
             if(!listCathed.contains(mapped)){
                 val itemAux = mapped
                 itemAux.quantity = listCathed.filter { it?.idItem ?:  0 == mapped.idItem ?: 0 }.size
                 listCathed.add(itemAux)
             }
return  listCathed

         }
//         var dashboardActivity = (context as? DashboardActivity)
//         var articleMap = HashMap<Long?, ArrayList<ArticleItem>?>()
//         if(dashboardActivity?.dashboardViewModel?.isShoppingBagActive == true){
//             if(articleMap.containsKey(article.idItem)){
//                 val arrayItems =   articleMap[article.idItem]
//                 if(arrayItems?.size ?: 0 > 0){
//                     arrayItems?.removeLast()
//                     articleMap.set(article.idItem, arrayItems)
//                 } else {
//                     articleMap.remove(article.idItem)
//                 }
//             }
//         }else{
//             if(articleMap.containsKey(article.idItem)){
//                 val arrayItems =   articleMap[article.idItem]
//                 arrayItems?.add(article)
//                 articleMap.set(article.idItem, arrayItems)
//             } else {
//                 articleMap.set(article.idItem, arrayListOf(article))
//             }
//         }
//
//         var listCathed = ArrayList<ArticleItem?>()
//         for (mapped in articleMap) {
//             val itemAux = mapped.value?.first()
//             itemAux?.quantity =  mapped.value?.size ?: 0
//             listCathed.add(itemAux)
//         }
//         return  listCathed
     }*/

    override fun getItemCount(): Int {
        return articles.size
    }
}