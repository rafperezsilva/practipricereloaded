package com.rafperezsi.practiprice

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.*
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

val mapper = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(Include.NON_NULL)
}
data class CodeItemModel (
    val itemsCoded: ArrayList<ItemModel>? = null
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<CodeItemModel>(json)
    }
}
@JsonIgnoreProperties(ignoreUnknown = true)
data class ItemModel (
    @get:JsonProperty("id_code")@field:JsonProperty("id_code")
    var idCode: Long? = null,
    @get:JsonProperty("item_name")@field:JsonProperty("item_name")
    var itemName: String? = null,
    @get:JsonProperty("item_description")@field:JsonProperty("item_description")
    var itemDescription: String? = null,
    var price: Double? = null,
    var discount: Double? = null,
    @get:JsonProperty("imagen_url")@field:JsonProperty("imagen_url")
    var imagenUrl: String? = null,
    var imageBase: String? = null

)


class StaticUtil {
    val item1 = ItemModel(75903169,"Belmont","Caja grande de 20 cigarrillos", 1.9,0.0,"https://cdn.shopify.com/s/files/1/0272/6730/1424/products/IMG_20200121_230857_300x300.jpg")
    val item2 = ItemModel(8681248502154,"Nutella","Envase de vidrio de 350g", 1.0,0.0,"https://casamagi.com/233/nutella-200gr.jpg")
    val item3 = ItemModel(7591002000011,"Harina PAN","Empaque de 1kg",1.5, 0.0,"shorturl.at/adV12")
    val item4 = ItemModel(75930868,"Margarina MAVESA","Empaque de 250gr",1.25, 0.0,"shorturl.at/gzHR0")
    val wiredData: ArrayList<ItemModel> = arrayListOf(item1,item2,item3,item4)
}