package com.rafperezsi.practiprice

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.rafperezsi.practiprice.utils.BaseActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN

        )
    }

    override fun onStart() {
        super.onStart()
        goToLogin()
    }

    private fun goToLogin() {
        Handler().postDelayed({
        this.startActivity(Intent(this, DashboardActivity::class.java))
        this.finish()
        }, 3500)
    }
}