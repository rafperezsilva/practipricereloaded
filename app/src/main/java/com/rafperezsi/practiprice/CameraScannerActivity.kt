package com.rafperezsi.practiprice

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import com.budiyev.android.codescanner.*
import com.rafperezsi.practiprice.article.ArticleActivity
import com.rafperezsi.practiprice.utils.BaseActivity
import com.rafperezsi.practiprice.utils.BaseConstants


class CameraScannerActivity : BaseActivity() {
    private lateinit var codeScanner: CodeScanner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_scanner)
        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)
        val targetView = findViewById<RelativeLayout>(R.id.target_layout)
        val myFadeInAnimation: Animation = AnimationUtils.loadAnimation(this, R.anim.fade_blink_anim)
        targetView.startAnimation(myFadeInAnimation)

        codeScanner = CodeScanner(this, scannerView)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ONE_DIMENSIONAL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.CONTINUOUS // or CONTINUOUS
        codeScanner.scanMode = ScanMode.CONTINUOUS // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                //playSound()
                it.text.toLongOrNull()?.let{idCode ->
//                    dashbordViewModel?.codeBarObeserver = CodeBarScannerObserver(dashbordViewModel ?: return@let)
//                    dashbordViewModel?.codeBarObeserver?.dataFromCodeBar = idCode
                    showMessage("Scan result: ${idCode}")

                    val data = Intent(this, ArticleActivity::class.java)
                    data.putExtra(BaseConstants.IntentKeys.IS_LOGIN_VIEW.name, idCode)
                    setResult(Activity.RESULT_OK, data)
                    finish()

                }
               showMessage("Scan result: ${it.text.toLongOrNull()}")
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            runOnUiThread {
                showMessage("Camera initialization error: ${it.message}")
            }
        }

        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        val data = Intent(this, ArticleActivity::class.java)
        data.putExtra(BaseConstants.IntentKeys.IS_LOGIN_VIEW.name, 123456)
        setResult(Activity.RESULT_OK, data)
        super.onPause()
    }

}