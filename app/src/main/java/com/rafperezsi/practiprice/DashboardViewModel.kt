package com.rafperezsi.practiprice

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.*
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fangxu.allangleexpandablebutton.AllAngleExpandableButton
import com.fangxu.allangleexpandablebutton.ButtonData
import com.fangxu.allangleexpandablebutton.ButtonEventListener
import com.rafperezsi.practiprice.article.*
import com.rafperezsi.practiprice.utils.BaseConstants
import com.rafperezsi.practiprice.utils.PreferenceTypeKey
import com.rafperezsi.practiprice.utils.requestCameraPermission


class DashboardViewModel(private val context: DashboardActivity):  ArticleDelegate, View.OnClickListener,
    ButtonEventListener {

    var isSearchViewActive = false
    var isShoppingBagActive = false

    init {
        initMainArticles()
        initSearchByCode()
        dashboardButtonsSettings()
    }

    fun initMainArticles(){
        updateBagButton()
        Preferences.getArticlesStored(context)?.articles?.let {
            initArticleList(it)
            return
        }
        ArticleRest(context).getArticleList(ArticleObserver(this))
    }

    fun dashboardButtonsSettings(){
         //context.findViewById<ImageButton>(R.id.add_article_image_button).setOnClickListener(this)
         context.findViewById<ImageButton>(R.id.add_article_image_button).setOnClickListener(this)
         context.findViewById<ImageButton>(R.id.retreive_list_image_button).setOnClickListener(this)
         initSearchViewSettings()


        val button = context.findViewById(R.id.button_expandable) as AllAngleExpandableButton
        val buttonDatas: MutableList<ButtonData> = ArrayList()
        val drawable =
            intArrayOf(R.drawable.ic_baseline_menu_24, R.drawable.ic_baseline_refresh_24, R.drawable.ic_baseline_qr_code_24, R.drawable.ic_baseline_add_shopping_cart_24)
        for (i in drawable.indices) {
            val buttonData = ButtonData.buildIconButton(context, drawable[i], 15f)
            buttonData.backgroundColor = context.resources.getColor(R.color.teal_999)

            buttonDatas.add(buttonData)
        }
        button.buttonDatas = buttonDatas
        button.setButtonEventListener(this)
     }

    fun initSearchViewSettings(){
        if(!isShoppingBagActive){
            isSearchViewActive = false
        }
        (context.findViewById(R.id.welcome_textview) as? TextView)?.text = if(isShoppingBagActive){"Carrito"}else{"Artículos Disponibles"}
        val searchViewContainer = context.findViewById<RelativeLayout>(R.id.searchview_container)
        context.findViewById<ImageButton>(R.id.search_open_button).setOnClickListener(this)
        searchViewContainer.animate().
            // translationX(20f).
        translationX((context.screenWidth).toFloat()).setDuration(0).start()
        context.findViewById<ImageButton>(R.id.search_open_button).alpha = if(isShoppingBagActive){0f}else{1f}
        context.findViewById<ImageButton>(R.id.search_open_button).isEnabled = !isShoppingBagActive
    }

    private fun searchViewSetings() {
        isSearchViewActive = !isSearchViewActive
        initSearchView()
        if(!isSearchViewActive) {
            context.findViewById<SearchView>(R.id.search_view_articles).setQuery("", false)
            context.findViewById<SearchView>(R.id.search_view_articles).clearFocus()
        } else {
            context.findViewById<SearchView>(R.id.search_view_articles).requestFocus()
        }
        context.findViewById<ImageButton>(R.id.search_open_button).setImageResource(
            if (isSearchViewActive) {
                R.drawable.ic_baseline_search_off_24
            } else {
                R.drawable.ic_baseline_youtube_searched_for_24
            }
        )
        initArticleList(
            if (isSearchViewActive) {
                null
            } else {
                Preferences.getArticlesStored(context)?.articles
            }
        )
        context.findViewById<RelativeLayout>(R.id.searchview_container).animate().
            //  translationX(20f).ic_baseline_youtube_searched_for_24
        translationX(
            if (isSearchViewActive) {
                (context.screenHeigth * 0.0115).toFloat()
            } else {
                context.screenWidth.toFloat()
            }
        ).setDuration(350).start()

    }

    fun updateBagButton(){
        val bagCounterTV = (context.findViewById(R.id.articles_bag_counter_tv) as? TextView)
        val bagButton =    (context.findViewById(R.id.articles_on_bag_button) as? ImageButton)
        val globalPrice =  (context.findViewById(R.id.price_global_TV) as? TextView)
        val animation = AnimationUtils.loadAnimation(context, R.anim.bounce)
        val shoppingBag = Preferences.getBagArticlesStored(context)
        // bagButton?.alpha = if(shoppingBag?.articles.isNullOrEmpty()) { 0f} else {1f}
        globalPrice?.startAnimation(animation)
        Handler().postDelayed({
            bagCounterTV?.startAnimation(animation)
        }, 250)

        bagButton?.isEnabled = !shoppingBag?.articles.isNullOrEmpty()
        bagCounterTV?.alpha = if(shoppingBag?.articles.isNullOrEmpty()) { 0f} else {1f}
        bagCounterTV?.text = "${shoppingBag?.articles?.size ?: 0}"
        globalPrice?.text = getPriceAtBag(shoppingBag?.articles)
        bagButton?.setOnClickListener(this)


        if (isShoppingBagActive){
            initArticleList(counterBaggingMaped()?.toList() as List<ArticleItem>? ?: return)
        }
        context.findViewById<RecyclerView>(R.id.dashboard_recycler_view).animate().translationY(
            if (isSearchViewActive) {
                (context.screenHeigth * 0.095).toFloat()
            } else {
                (context.screenHeigth * 0.008).toFloat()
            }
        ).setDuration(350).start()
        initSearchViewSettings()
        (context.findViewById(R.id.articles_on_bag_button) as? ImageButton)?.setImageResource(
            if (isShoppingBagActive) {
                R.drawable.ic_baseline_arrow_back_24
            } else {
                R.drawable.ic_baseline_shopping_cart_24
            }
        )

    }
    fun counterBaggingMaped() :ArrayList<ArticleItem?>? {
        val articles = Preferences.getBagArticlesStored(context)?.articles ?: return null
        val listCathed = ArrayList<ArticleItem?>()
        for (mapped in articles) {

            if(listCathed.filter { it?.idItem  == mapped.idItem}.isEmpty()){
                val itemAux = mapped
                itemAux.quantity = articles.filter { it?.idItem ?:  0 == mapped.idItem ?: 0 }.size
                listCathed.add(itemAux)
            }


        }
        return  listCathed
    }
    fun getPriceAtBag(articles: ArrayList<ArticleItem>?):String{
        var price: Float = 0f
        articles?.let {

            for (article in it) {
                price +=  article.priceItem  ?: 0f
            }
            return "$${String.format("%.2f", price)}"
        }
        return  "$0.0"
    }

    override fun onArticleAdded(isAdded: Boolean) {
    }

    override fun onArticlesRetrieved(articles: ArticleListModel) {
        initArticleList(articles.articles)
        Preferences.saveDataPreference(context, PreferenceTypeKey.ARTICLES, articles.toJson())
    }

    override fun onClick(v: View?) {
        val animation = AnimationUtils.loadAnimation(context, R.anim.bounce_in)
        (context.findViewById(v?.id ?: 0) as? ImageButton)?.startAnimation(animation)
        when(v?.id){
            R.id.search_by_code_button -> {
                if (context.requestCameraPermission(context, BaseConstants.PERMISSION_CODE_CAMERA))
                    context.startActivityForResult(
                        Intent(
                            context,
                            CameraScannerActivity::class.java
                        ), BaseConstants.SERVICO_DETALHES_REQUEST
                    )
            }
            R.id.add_article_image_button -> {
                context.startActivity(Intent(context, ArticleActivity::class.java))
            }
            R.id.retreive_list_image_button -> {
                ArticleRest(context).getArticleList(ArticleObserver(this))
            }
            R.id.search_open_button -> {
                searchViewSetings()
            }
            R.id.articles_on_bag_button -> {
                isShoppingBagActive = !isShoppingBagActive
                (context.findViewById(v?.id ?: 0) as? ImageButton)?.setImageResource(
                    if (isShoppingBagActive) {
                        R.drawable.ic_baseline_arrow_back_24
                    } else {
                        R.drawable.ic_baseline_shopping_cart_24
                    }
                )
                initArticleList(
                    if (isShoppingBagActive) {
                        counterBaggingMaped()?.toList() as List<ArticleItem>? ?: return
                    } else {
                        Preferences.getArticlesStored(
                            context
                        )?.articles
                    }
                )
                initSearchViewSettings()
            }
        }
    }

    private fun initArticleList(articles: List<ArticleItem>?){
        val recyclerView =  context.findViewById<RecyclerView>(R.id.dashboard_recycler_view)
        recyclerView.animate().translationY(
            if (isSearchViewActive) {
                (context.screenHeigth * 0.1).toFloat()
            } else {
                (context.screenHeigth * 0.008).toFloat()
            }
        ).setDuration(350).start()
        val adapter = ArticlesAdapter(articles ?: return, context)
        val mLayoutManager = LinearLayoutManager(context)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = mLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = adapter
    }

    private fun initSearchView(){
        val searchView =
            context.findViewById<SearchView>(R.id.search_view_articles)

        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            Log.i("SEARCH VIEW", "HAS FOCUS $hasFocus")
        }
        searchView?.setOnCloseListener {
            context.hideKeyboard()
            searchView.clearFocus()
            true
        }
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }


            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText == "") {
                    context.hideKeyboard()
                    //    screenDimensionManager(LocationViewType.EXPANDED_TYPE)
                } else {
                    // screenDimensionManager(LocationViewType.SEARCH_TYPE)
                }
                val locations = Preferences.getArticlesStored(context)?.articles
                locations?.let { filterQueryData(it as ArrayList<ArticleItem>, newText.toString()) }
                    ?.let {
                        initArticleList(it)
                    }
                return true
            }

        })
    }

    private fun  initSearchByCode(){
        (context.findViewById(R.id.search_by_code_button) as? ImageButton)?.setOnClickListener(this)
    }

    internal  fun getArticleFromCode(idCode: Long?){

        Preferences.getArticlesStored(context)?.articles?.let{
            val itemMatched = it.filter { it.codeItem == "${idCode ?: return}" }
            if(itemMatched.isNullOrEmpty()){
                    showAlertProfileDialog()
            }else{
                    initArticleList(itemMatched)
            }
        }
    }
    fun filterQueryData(
        locations: ArrayList<ArticleItem>,
        query: String
    ): List<ArticleItem> {
        return locations.filter {
            it.nameItem?.toLowerCase()?.contains(query) ?: false ||
                    it.descriptionItem?.toLowerCase()?.contains(query) ?: false ||
                    it.codeItem?.toLowerCase()?.contains(query) ?: false
        }
    }

    fun  showAlertProfileDialog() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(context)
        alertDialog.setTitle("Uuups!")
        // Setting Dialog Message
        alertDialog.setCancelable(false)
        alertDialog.setMessage("No contamos con este artículo en nuestro sistema, por favor ayúdanos agregándolo")
        alertDialog.setNegativeButton(
            "Omitir"
        ) { dialog, _ ->
dialog.cancel()
        }
        alertDialog.setPositiveButton("Agregar",
            DialogInterface.OnClickListener { _, _ ->
                context.startActivity(Intent(context, ArticleActivity::class.java))
            })
        alertDialog.show()
    }

    override fun onButtonClicked(index: Int) {
        Log.i("BUTTON EXPAND", "$index")
        when(index) {
            1 -> {
                ArticleRest(context).getArticleList(ArticleObserver(this))
            }
            2 -> {
                if (context.requestCameraPermission(context, BaseConstants.PERMISSION_CODE_CAMERA))
                    context.startActivityForResult(
                        Intent(
                            context,
                            CameraScannerActivity::class.java
                        ), BaseConstants.SERVICO_DETALHES_REQUEST
                    )
            }
            3 ->{
                context.startActivity(Intent(context, ArticleActivity::class.java))
            }
        }
    }

    override fun onExpand() {
    }

    override fun onCollapse() {
    }
}

/*
/**
 * Created by Anshul on 24/06/15.
 */
class GooeyMenu : View {
    private val START_ANGLE = 0f
    private val END_ANGLE = 45f
    private var mNumberOfMenu //Todo
            = 0
    private val BEZIER_CONSTANT = 0.551915024494f // pre-calculated value
    private var mFabButtonRadius = 0
    private var mMenuButtonRadius = 0
    private var mGab = 0
    private var mCenterX = 0
    private var mCenterY = 0
    private var mCirclePaint: Paint? = null
    private val mMenuPoints = java.util.ArrayList<CirclePoint>()
    private val mShowAnimation = java.util.ArrayList<ObjectAnimator>()
    private var mHideAnimation: java.util.ArrayList<ObjectAnimator>? = java.util.ArrayList()
    private var mBezierAnimation: ValueAnimator? = null
    private var mBezierEndAnimation: ValueAnimator? = null
    private var mRotationAnimation: ValueAnimator? = null
    private var isMenuVisible = true
    private var bezierConstant = BEZIER_CONSTANT
    private var mPlusBitmap: Bitmap? = null
    private var mRotationAngle = 0f
    private var mRotationReverseAnimation: ValueAnimator? = null
    private var mGooeyMenuInterface: GooeyMenuInterface? = null
    private val gooeyMenuTouch = false
    private var mCircleBorder: Paint? = null
    private var mDrawableArray: MutableList<Drawable>? = null

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            var typedArray: TypedArray? = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.GooeyMenu,
                0, 0
            )
            try {
                mNumberOfMenu =
                    typedArray!!.getInt(R.styleable.GooeyMenu_no_of_menu, DEFUALT_MENU_NO)
                mFabButtonRadius =
                    typedArray.getDimension(
                        R.styleable.GooeyMenu_fab_radius,
                        resources.getDimension(R.dimen.big_circle_radius)
                    )
                        .toInt()
                mMenuButtonRadius =
                    typedArray.getDimension(
                        R.styleable.GooeyMenu_menu_radius,
                        resources.getDimension(R.dimen.small_circle_radius)
                    )
                        .toInt()
                mGab = typedArray.getDimension(
                    R.styleable.    GooeyMenu_gap_between_menu_fab,
                    resources.getDimensionPixelSize(R.dimen.min_gap).toFloat()
                )
                    .toInt()
                val outValue = TypedValue()
                // Read array of target drawables
                if (typedArray.getValue(R.styleable.GooeyMenu_menu_drawable, outValue)) {
                    val res = context.resources
                    val array = res.obtainTypedArray(outValue.resourceId)
                    mDrawableArray = java.util.ArrayList(array.length())
                    for (i in 0 until array.length()) {
                        val value = array.peekValue(i)
                        (mDrawableArray as java.util.ArrayList<Drawable>).add(resources.getDrawable(value?.resourceId ?: 0))
                    }
                    array.recycle()
                }
            } finally {
                typedArray!!.recycle()
                typedArray = null
            }
        }
        mCirclePaint = Paint()
        mCirclePaint!!.color = resources.getColor(R.color.teal_400)
        mCirclePaint!!.style = Paint.Style.FILL_AND_STROKE
        mCircleBorder = Paint(mCirclePaint)
        mCircleBorder!!.style = Paint.Style.STROKE
        mCircleBorder!!.strokeWidth = 1f
        mCircleBorder!!.color = resources.getColor(R.color.teal_200)
        mBezierEndAnimation = ValueAnimator.ofFloat(BEZIER_CONSTANT + .2f, BEZIER_CONSTANT)
        mBezierEndAnimation?.setInterpolator(LinearInterpolator())
        mBezierEndAnimation?.setDuration(300)
        mBezierEndAnimation?.addUpdateListener(mBezierUpdateListener)
        mBezierAnimation = ValueAnimator.ofFloat(BEZIER_CONSTANT - .02f, BEZIER_CONSTANT + .2f)
        mBezierAnimation?.setDuration(ANIMATION_DURATION / 4)
        mBezierAnimation?.setRepeatCount(4)
        mBezierAnimation?.setInterpolator(LinearInterpolator())
        mBezierAnimation?.addUpdateListener(mBezierUpdateListener)
        mBezierAnimation?.addListener(mBezierAnimationListener)
        mRotationAnimation = ValueAnimator.ofFloat(START_ANGLE, END_ANGLE)
        mRotationAnimation?.setDuration(ANIMATION_DURATION / 4)
        mRotationAnimation?.setInterpolator(AccelerateInterpolator())
        mRotationAnimation?.addUpdateListener(mRotationUpdateListener)
        mRotationReverseAnimation = ValueAnimator.ofFloat(END_ANGLE, START_ANGLE)
        mRotationReverseAnimation?.setDuration(ANIMATION_DURATION / 4)
        mRotationReverseAnimation?.setInterpolator(AccelerateInterpolator())
        mRotationReverseAnimation?.addUpdateListener(mRotationUpdateListener)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val desiredWidth: Int
        val desiredHeight: Int
        desiredWidth = measuredWidth
        desiredHeight = context.resources.getDimensionPixelSize(R.dimen.min_height)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val width: Int
        val height: Int
        //Measure Width
        width = if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            widthSize
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            Math.min(desiredWidth, widthSize)
        } else {
            //Be whatever you want
            desiredWidth
        }

        //Measure Height
        height = if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            heightSize
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            Math.min(desiredHeight, heightSize)
        } else {
            //Be whatever you want
            desiredHeight
        }
        setMeasuredDimension(width, height)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mCenterX = w / 2
        mCenterY = h - mFabButtonRadius
        for (i in 0 until mNumberOfMenu) {
            val circlePoint: CirclePoint = CirclePoint()
            circlePoint.radius = mGab.toFloat()
            circlePoint.angle = Math.PI / (mNumberOfMenu + 1) * (i + 1)
            mMenuPoints.add(circlePoint)
            val animShow = ObjectAnimator.ofFloat(mMenuPoints[i], "Radius", 0f, mGab.toFloat())
            animShow.duration = ANIMATION_DURATION
            animShow.interpolator = AnticipateOvershootInterpolator()
            animShow.startDelay = ANIMATION_DURATION * (mNumberOfMenu - i) / 10
            animShow.addUpdateListener(mUpdateListener)
            mShowAnimation.add(animShow)
            val animHide = animShow.clone()
            animHide.setFloatValues(mGab.toFloat(), 0f)
            animHide.startDelay = ANIMATION_DURATION * i / 10
            mHideAnimation!!.add(animHide)
            if (mDrawableArray != null) {
                for (drawable in mDrawableArray!!) drawable.setBounds(
                    0,
                    0,  /*2 * */
                    mMenuButtonRadius,  /* 2 * */
                    mMenuButtonRadius
                )
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        mPlusBitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_note_add_24)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mPlusBitmap = null
        mBezierAnimation = null
        mHideAnimation!!.clear()
        mHideAnimation = null
        mShowAnimation.clear()
        mHideAnimation = null
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        for (i in 0 until mNumberOfMenu) {
            val circlePoint = mMenuPoints[i]
            val x = (circlePoint.radius * Math.cos(circlePoint.angle)).toFloat()
            val y = (circlePoint.radius * Math.sin(circlePoint.angle)).toFloat()
            canvas.drawCircle(
                x + mCenterX, mCenterY - y, mMenuButtonRadius.toFloat(),
                mCirclePaint!!
            )
            if (i < mDrawableArray!!.size) {
                canvas.save()
                canvas.translate(
                    x + mCenterX - mMenuButtonRadius / 2,
                    mCenterY - y - mMenuButtonRadius / 2
                )
                mDrawableArray!![i].draw(canvas)
                canvas.restore()
            }
        }
        canvas.save()
        canvas.translate(mCenterX.toFloat(), mCenterY.toFloat())
        val path = createPath()
        canvas.drawPath(path, mCirclePaint!!)
        canvas.drawPath(path, mCircleBorder!!)
        canvas.rotate(mRotationAngle)
        canvas.drawBitmap(
            mPlusBitmap!!,
            (-mPlusBitmap!!.width / 2).toFloat(),
            (-mPlusBitmap!!.height / 2).toFloat(),
            mCirclePaint
        )
        canvas.restore()
    }

    // Use Bezier path to create circle,
    /*    P_0 = (0,1), P_1 = (c,1), P_2 = (1,c), P_3 = (1,0)
        P_0 = (1,0), P_1 = (1,-c), P_2 = (c,-1), P_3 = (0,-1)
        P_0 = (0,-1), P_1 = (-c,-1), P_3 = (-1,-c), P_4 = (-1,0)
        P_0 = (-1,0), P_1 = (-1,c), P_2 = (-c,1), P_3 = (0,1)
        with c = 0.551915024494*/
    private fun createPath(): Path {
        val path = Path()
        val c = bezierConstant * mFabButtonRadius
        path.moveTo(0f, mFabButtonRadius.toFloat())
        path.cubicTo(
            bezierConstant * mFabButtonRadius,
            mFabButtonRadius.toFloat(),
            mFabButtonRadius.toFloat(),
            BEZIER_CONSTANT * mFabButtonRadius,
            mFabButtonRadius.toFloat(),
            0f
        )
        path.cubicTo(
            mFabButtonRadius.toFloat(),
            BEZIER_CONSTANT * mFabButtonRadius * -1,
            c,
            (-1 * mFabButtonRadius).toFloat(),
            0f,
            (-1 * mFabButtonRadius).toFloat()
        )
        path.cubicTo(
            -1 * c,
            (-1 * mFabButtonRadius).toFloat(),
            (-1 * mFabButtonRadius).toFloat(),
            -1 * BEZIER_CONSTANT * mFabButtonRadius,
            (-1 * mFabButtonRadius).toFloat(),
            0f
        )
        path.cubicTo(
            (-1 * mFabButtonRadius).toFloat(),
            BEZIER_CONSTANT * mFabButtonRadius,
            -1 * bezierConstant * mFabButtonRadius,
            mFabButtonRadius.toFloat(),
            0f,
            mFabButtonRadius.toFloat()
        )
        return path
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (isGooeyMenuTouch(event)) {
                    return true
                }
                val menuItem = isMenuItemTouched(event)
                if (isMenuVisible && menuItem > 0) {
                    if (menuItem <= mDrawableArray!!.size) {
                        mDrawableArray!![mMenuPoints.size - menuItem].state = STATE_PRESSED
                        invalidate()
                    }
                    return true
                }
                return false
            }
            MotionEvent.ACTION_UP -> {
                if (isGooeyMenuTouch(event)) {
                    mBezierAnimation!!.start()
                    cancelAllAnimation()
                    if (isMenuVisible) {
                        startHideAnimate()
                        if (mGooeyMenuInterface != null) {
                            mGooeyMenuInterface!!.menuClose()
                        }
                    } else {
                        startShowAnimate()
                        if (mGooeyMenuInterface != null) {
                            mGooeyMenuInterface!!.menuOpen()
                        }
                    }
                    isMenuVisible = !isMenuVisible
                    return true
                }
                if (isMenuVisible) {
                var menuItem = isMenuItemTouched(event)
                    invalidate()
                    if (menuItem > 0) {
                        if (menuItem <= mDrawableArray!!.size) {
                            mDrawableArray!![mMenuPoints.size - menuItem].state = STATE_ACTIVE
                            postInvalidateDelayed(1000)
                        }
                        if (mGooeyMenuInterface != null) {
                            mGooeyMenuInterface!!.menuItemClicked(menuItem)
                        }
                        return true
                    }
                }
                return false
            }
        }
        return true
    }

    private fun isMenuItemTouched(event: MotionEvent): Int {
        if (!isMenuVisible) {
            return -1
        }
        for (i in mMenuPoints.indices) {
            val circlePoint = mMenuPoints[i]
            val x = (mGab * Math.cos(circlePoint.angle)).toFloat() + mCenterX
            val y = mCenterY - (mGab * Math.sin(circlePoint.angle)).toFloat()
            if (event.x >= x - mMenuButtonRadius && event.x <= x + mMenuButtonRadius) {
                if (event.y >= y - mMenuButtonRadius && event.y <= y + mMenuButtonRadius) {
                    return mMenuPoints.size - i
                }
            }
        }
        return -1
    }

    fun setOnMenuListener(onMenuListener: GooeyMenuInterface?) {
        mGooeyMenuInterface = onMenuListener
    }

    fun isGooeyMenuTouch(event: MotionEvent): Boolean {
        if (event.x >= mCenterX - mFabButtonRadius && event.x <= mCenterX + mFabButtonRadius) {
            if (event.y >= mCenterY - mFabButtonRadius && event.y <= mCenterY + mFabButtonRadius) {
                return true
            }
        }
        return false
    }

    // Helper class for animation and Menu Item cicle center Points
    inner class CirclePoint {
        var x = 0f
        var y = 0f
        var radius = 0.0f
        var angle = 0.0
    }

    private fun startShowAnimate() {
        mRotationAnimation!!.start()
        for (objectAnimator in mShowAnimation) {
            objectAnimator.start()
        }
    }

    private fun startHideAnimate() {
        mRotationReverseAnimation!!.start()
        for (objectAnimator in mHideAnimation!!) {
            objectAnimator.start()
        }
    }

    private fun cancelAllAnimation() {
        for (objectAnimator in mHideAnimation!!) {
            objectAnimator.cancel()
        }
        for (objectAnimator in mShowAnimation) {
            objectAnimator.cancel()
        }
    }

    var mUpdateListener = AnimatorUpdateListener { invalidate() }
    var mBezierUpdateListener =
        AnimatorUpdateListener { valueAnimator ->
            bezierConstant = valueAnimator.animatedValue as Float
            invalidate()
        }
    var mRotationUpdateListener =
        AnimatorUpdateListener { valueAnimator ->
            mRotationAngle = valueAnimator.animatedValue as Float
            invalidate()
        }
    var mBezierAnimationListener: Animator.AnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationStart(animator: Animator) {}
        override fun onAnimationEnd(animator: Animator) {
            mBezierEndAnimation!!.start()
        }

        override fun onAnimationCancel(animator: Animator) {}
        override fun onAnimationRepeat(animator: Animator) {}
    }

    interface GooeyMenuInterface {
        /**
         * Called when menu opened
         */
        fun menuOpen()

        /**
         * Called when menu Closed
         */
        fun menuClose()

        /**
         * Called when Menu item Clicked
         *
         * @param menuNumber give menu number which clicked.
         */
        fun menuItemClicked(menuNumber: Int)
    }

    companion object {
        private const val ANIMATION_DURATION: Long = 1000
        private const val DEFUALT_MENU_NO = 5
        val STATE_ACTIVE = intArrayOf(0,1)
        val STATE_PRESSED = intArrayOf(
           0,1,2
        )
    }
}
*/