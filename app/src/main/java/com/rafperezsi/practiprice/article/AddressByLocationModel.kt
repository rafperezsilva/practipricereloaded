

package com.rafperezsi.practiprice.article

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.module.kotlin.*

val mapper = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class AddressByLocationModel (
    val latitude: Double? = null,
    val longitude: Double? = null,
    val lookupSource: String? = null,
    val plusCode: String? = null,
    val localityLanguageRequested: String? = null,
    val continent: String? = null,
    val continentCode: String? = null,
    val countryName: String? = null,
    val countryCode: String? = null,
    val principalSubdivision: String? = null,
    val principalSubdivisionCode: String? = null,
    val city: String? = null,
    val locality: String? = null,
    val postcode: String? = null,
    val localityInfo: LocalityInfo? = null
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<AddressByLocationModel>(json)
    }
}

data class LocalityInfo (
    val administrative: List<Ative>? = null,
    val informative: List<Ative>? = null
)

data class Ative (
    val order: Long? = null,
    val adminLevel: Long? = null,
    val name: String? = null,
    val description: String? = null,

    @get:JsonProperty("isoName")@field:JsonProperty("isoName")
    val isoName: String? = null,

    @get:JsonProperty("isoCode")@field:JsonProperty("isoCode")
    val isoCode: String? = null,

    @get:JsonProperty("wikidataId")@field:JsonProperty("wikidataId")
    val wikidataID: String? = null,

    @get:JsonProperty("geonameId")@field:JsonProperty("geonameId")
    val geonameID: Long? = null
)
