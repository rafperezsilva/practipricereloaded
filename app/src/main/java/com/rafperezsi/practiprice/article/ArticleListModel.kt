package com.rafperezsi.practiprice.article


import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.module.kotlin.*
import com.rafperezsi.practiprice.mapper

data class ArticleListModel (
        var articles: ArrayList<ArticleItem>? = null
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<ArticleListModel>(json)
    }
}

data class ArticleItem (
        @get:JsonProperty("id_item")@field:JsonProperty("id_item")
        var idItem: Long? = null,

        var quantity: Int? = null,
        var latitude:String? = null,
        var longitude:String? = null ,
        @get:JsonProperty("name_item")@field:JsonProperty("name_item")
        var nameItem: String? = null,

        @get:JsonProperty("description_item")@field:JsonProperty("description_item")
        var descriptionItem: String? = null,

        @get:JsonProperty("code_item")@field:JsonProperty("code_item")
        var codeItem: String? = null,

        @get:JsonProperty("price_item")@field:JsonProperty("price_item")
        var priceItem: Float? = null,
        var priceDoubleItem: Double? = priceItem?.toDouble() ?: 0.0,

        @get:JsonProperty("discount_item")@field:JsonProperty("discount_item")
        var discountItem: Float? = 0f,
        var discountDoubleItem: Double? = discountItem?.toDouble() ?: 0.0,

        @get:JsonProperty("image_item")@field:JsonProperty("image_item")
        var imageItem: String? = null,

    @get:JsonProperty("located")@field:JsonProperty("located")
     var located: String? = null
){
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<ArticleItem>(json)
    }
}
