package com.rafperezsi.practiprice.article

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.os.Build
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.widget.doOnTextChanged
import com.bluecoltan.dottys.utils.extensions.encodeToBitmap
import com.bluecoltan.dottys.utils.extensions.roundOffDecimal
import com.rafperezsi.practiprice.CameraScannerActivity
import com.rafperezsi.practiprice.R
import com.rafperezsi.practiprice.utils.BaseActivity
import com.rafperezsi.practiprice.utils.BaseConstants
import com.rafperezsi.practiprice.utils.requestCameraPermission
import kotlin.reflect.full.memberProperties

class ArticleAddViewModel(private val context:BaseActivity) : View.OnClickListener,
    View.OnFocusChangeListener, ArticleDelegate {
    enum class InputType {ITEM_NAME, ITEM_DESCRIPTION, ITEM_IMAGE, ITEM_CODE, ITEM_COMPLETE}
    var itemModel = ArticleItem()
    var typeItemView =  InputType.ITEM_NAME

    init {
        checkPermission()
        context.findViewById<ImageButton>(R.id.button_spell_item_IB).setOnClickListener(this)
        context.findViewById<Button>(R.id.done_text_submit_article_iv).setOnClickListener(this)
        context.findViewById<Button>(R.id.add_new_article_button).setOnClickListener(this)
        context.findViewById<ImageButton>(R.id.code_scan_image_button).setOnClickListener(this)
        val priceET = (context.findViewById(R.id.price_item_et) as EditText)
        priceET.onFocusChangeListener = this
        val disscountET = (context.findViewById(R.id.disscount_item_et) as EditText)
        disscountET.onFocusChangeListener = this
        disscountET.doOnTextChanged { charSequence: CharSequence?, i: Int, i1: Int, i2: Int ->
            if(charSequence?.length ?: 0 > 0) {
                itemModel.discountItem = charSequence.toString().toFloat()
            }  else {
                itemModel.discountItem = null
            }
            validateDataItem()
        }
        priceET.doOnTextChanged { charSequence: CharSequence?, i: Int, i1: Int, i2: Int ->
            if(charSequence?.length ?: 0 > 0) {
                itemModel.priceItem = charSequence.toString().toFloat()
            } else {
                itemModel.priceItem = null
            }
            validateDataItem()
        }
        validateDataItem()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.article_image_view ->{
                typeItemView = InputType.ITEM_IMAGE
            }
            R.id.name_article ->{
                typeItemView = InputType.ITEM_NAME
            }
            R.id.description_article ->{
                typeItemView = InputType.ITEM_DESCRIPTION
            }
            R.id.code_article ->{
                typeItemView = InputType.ITEM_CODE
            }
            R.id.add_new_article_button ->{
                ArticleRest(context).addNewArticle(itemModel, ArticleObserver(this))
            }
            R.id.button_spell_item_IB ->{}
            R.id.code_scan_image_button ->{
                if(typeItemView == InputType.ITEM_IMAGE) {
                    if (context.requestCameraPermission(
                            context,
                            BaseConstants.PERMISSION_CAPTURE_CAMERA
                        )
                    )
                        dispatchTakePictureIntent()
                } else {
                    if (context.requestCameraPermission(
                            context,
                            BaseConstants.PERMISSION_CODE_CAMERA
                        )
                    )
                        context.startActivityForResult(
                            Intent(
                                context,
                                CameraScannerActivity::class.java
                            ), BaseConstants.SERVICO_DETALHES_REQUEST
                        )
                }
            }
            R.id.done_text_submit_article_iv ->{
                val itemInputET = context.findViewById<EditText>(R.id.text_spelled_article_et)
                val itemInputText = itemInputET.text.toString()
                if(itemInputText.isEmpty()  && (typeItemView != InputType.ITEM_IMAGE || typeItemView != InputType.ITEM_COMPLETE)){
                    context.showMessage("Agrega ${if(typeItemView == InputType.ITEM_NAME) "Nombre" else "Descripcion"} de artículo")
                    return
                }
//                else if (typeItemView == InputType.ITEM_IMAGE || typeItemView == InputType.ITEM_COMPLETE){
//                    return
//                }
                else
                {
                    typeItemView = when(typeItemView){
                        InputType.ITEM_NAME -> {
                            itemModel.nameItem = itemInputText
                            InputType.ITEM_DESCRIPTION
                        }
                        InputType.ITEM_DESCRIPTION -> {
                            itemModel.descriptionItem = itemInputText
                            InputType.ITEM_CODE
                        }
                        InputType.ITEM_CODE -> {
                            itemModel.codeItem = itemInputText
                            InputType.ITEM_IMAGE
                        }
                        else -> {
                            //itemModel.descriptionItem = itemInputText
                            InputType.ITEM_COMPLETE
                        }
                    }
                }
                itemInputET.setText("")
                animateViewType()
            }
        }
        animateViewType()
        context.hideKeyboard()
    }


    fun animateViewType(){
        val itemInputET = context.findViewById<EditText>(R.id.text_spelled_article_et)

        val titleViewTV = context.findViewById<TextView>(R.id.title_input_item_TV)
        val imageScanIB = context.findViewById<ImageButton>(R.id.code_scan_image_button)
        context.findViewById<LinearLayout>(R.id.container_item_LL).visibility =  if(typeItemView == InputType.ITEM_IMAGE) View.GONE else View.VISIBLE
        context.findViewById<Button>(R.id.done_text_submit_article_iv).visibility =  if(typeItemView == InputType.ITEM_IMAGE) View.GONE else View.VISIBLE
        imageScanIB.visibility = if(typeItemView == InputType.ITEM_CODE || typeItemView == InputType.ITEM_IMAGE) View.VISIBLE else View.GONE
        imageScanIB.setImageResource(if(typeItemView == InputType.ITEM_CODE) R.drawable.ic_baseline_qr_code_24 else R.drawable.ic_baseline_image_24)
        titleViewTV.text =    when(typeItemView) {
            InputType.ITEM_NAME -> {
                itemInputET.setText(itemModel.nameItem ?: "")
                "Nombre del Artículo"
            }
            InputType.ITEM_CODE -> {
                itemInputET.setText(itemModel.codeItem ?: "")
                "Ingresa o Escanea el Código"
            }
            InputType.ITEM_IMAGE -> {
                imageScanIB.setOnLongClickListener {
                    openGalleryForImage()
                    return@setOnLongClickListener true
                }
                "Agrega una imagen del artículo"
            }
            InputType.ITEM_DESCRIPTION -> {
                itemInputET.setText(itemModel.descriptionItem ?: "")
                "Descripción del Artículo"
            }
            else -> {
                "Infomación completa"
            }
        }
        validateDataItem()
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(
                context,
                arrayOf(Manifest.permission.RECORD_AUDIO),
                BaseConstants.RECORD_AUDIO_REQUEST_CODE
            )
        }
    }

    private fun updateCompleteItems(){
        itemModel.imageItem?.let { (context.findViewById(R.id.article_image_view) as ImageView)?.setImageBitmap(it.encodeToBitmap())}
        (context.findViewById(R.id.name_article) as? TextView)?.text = itemModel.nameItem
        (context.findViewById(R.id.description_article) as? TextView)?.text =  itemModel.descriptionItem
        (context.findViewById(R.id.name_article) as? TextView)?.setOnClickListener(this)
        (context.findViewById(R.id.description_article) as? TextView)?.setOnClickListener(this)
        (context.findViewById(R.id.code_article) as? TextView)?.setOnClickListener(this)
        (context.findViewById(R.id.article_image_view) as? ImageView)?.setOnClickListener(this)
        (context.findViewById(R.id.code_article) as? TextView)?.text = "${itemModel.codeItem ?: ""}"
//        (context.findViewById(R.id.disscount_item_et) as? EditText)?.text?.let {
//            if(!it.toString().isNullOrEmpty())
//                try {
//
//                    itemModel.discountItem = it.toString().toDouble().roundOffDecimal()
//
//                }catch (e: Exception){
//                    Log.e("DISSCOUNT ERROR", "${e.message}")
//                }
//        }
//        (context.findViewById(R.id.price_item_et) as? EditText)?.text?.let{
//            if(!it.toString().isNullOrEmpty())
//                try {
//                    itemModel.priceItem = it.toString().toDouble().roundOffDecimal()
//                }  catch (e: Exception){
//                    Log.e("PRICE ERROR", "${e.message}")
//                }
//        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        validateDataItem()
    }

    private fun validateDataItem(){
        checkCompleteValues()
        val addItemButton = context.findViewById<Button>(R.id.add_new_article_button)
        val itemInputView = context.findViewById<View>(R.id.item_input_view)
        val itemComplete = context.findViewById<View>(R.id.article_info_layout_include)
        try {
            itemInputView.visibility =
                if (typeItemView == InputType.ITEM_COMPLETE) View.GONE else View.VISIBLE
            itemComplete.visibility =
                if (typeItemView == InputType.ITEM_COMPLETE) View.VISIBLE else View.GONE
        } catch (e:Exception){
            Log.e("VALIDATE DATA ITEMS","${e.message}")
        }
        updateCompleteItems()
        if(typeItemView == InputType.ITEM_COMPLETE && itemModel.priceItem != null) {
            addItemButton.visibility = View.VISIBLE
        } else {
            addItemButton.visibility = View.GONE
        }
    }

    private fun checkCompleteValues(){
        when {

        }
//        for (prop in ArticleItem::class.memberProperties) {
//            prop.name
//        }
    }

    private fun dispatchTakePictureIntent() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        context.imageUri = context.contentResolver?.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // cameraIntent.putExtra("android.intent.extra.", true);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, context.imageUri)
        context.startActivityForResult(cameraIntent, BaseConstants.IMAGE_CAPTURE_CODE)
    }

     fun openGalleryForImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        context.startActivityForResult(intent, BaseConstants.PICK_IMAGE_FROM_GALERY)
    }

    override fun onArticleAdded(isAdded: Boolean) {
        val msg = if (isAdded) {
            "El artículo ha sido agregado satisfactoriamente"
        } else {
            "Hubo un problema al ingresar el artículo, intente de nuevo por favor"
        }
        typeItemView = InputType.ITEM_NAME
        android.os.Handler(Looper.getMainLooper()).post {
            context.showMessage(msg)
//                (context.findViewById(R.id.add_new_article_button) as? Button)?.alpha = 1f
//                (context.findViewById(R.id.add_new_article_button) as? Button)?.isEnabled = true
            android.os.Handler().postDelayed({
                if (isAdded)
                    context.finish()
            }, 2000)
        }
        animateViewType()

    }

    override fun onArticlesRetrieved(articles: ArticleListModel) {
    }
}