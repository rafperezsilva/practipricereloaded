package com.rafperezsi.practiprice.article

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.speech.RecognitionListener
import android.speech.SpeechRecognizer
import android.util.Log
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.lyrebirdstudio.croppylib.Croppy
import com.lyrebirdstudio.croppylib.main.CropRequest
import com.rafperezsi.practiprice.CameraScannerActivity
import com.rafperezsi.practiprice.R
import com.rafperezsi.practiprice.utils.BaseActivity
import com.rafperezsi.practiprice.utils.BaseConstants
import com.rafperezsi.practiprice.utils.GpsTracker
import com.rafperezsi.practiprice.utils.encodeToString
import java.lang.Exception
import java.util.ArrayList

class ArticleActivity : BaseActivity(), RecognitionListener, PlaceDirectionsDelegates {

    var articleViewModel: ArticleAddViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)
        articleViewModel = ArticleAddViewModel(this)
        ArticleRest(this).getAddressByLocation(GpsTracker(this).locationGps ?: currentLocation ?: return, PlaceDirectionsObserver(this))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when {
            (requestCode == BaseConstants.RECORD_AUDIO_REQUEST_CODE && grantResults.isNotEmpty()) -> {
                 if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                     ArticleViewModel(this).initSpellRecognition()
                println("INIT SPELL RECOGNITION")
            }
            requestCode ==  BaseConstants.PERMISSION_CAPTURE_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    showMessage( "Camera permission granted")
                  //  articleViewModel?.dispatchTakePictureIntent()
                } else {
                    showMessage( "Camera permission denied")
                }
            }
            requestCode ==  BaseConstants.PERMISSION_CODE_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    showMessage( "Camera code permission granted")
                    startActivityForResult(Intent(this, CameraScannerActivity::class.java), BaseConstants.SERVICO_DETALHES_REQUEST)
                } else {
                    showMessage( "Camera code permission denied")
                }
            }

        }
    }

    override fun onStop() {
        super.onStop()
        //articleViewModel?.itemModel = ArticleItem()
    }
    override fun onReadyForSpeech(params: Bundle?) {
    }

    override fun onRmsChanged(rmsdB: Float) {
    }

    override fun onBufferReceived(buffer: ByteArray?) {
    }

    override fun onPartialResults(partialResults: Bundle?) {
    }

    override fun onEvent(eventType: Int, params: Bundle?) {
    }

    override fun onBeginningOfSpeech() {
    }

    override fun onEndOfSpeech() {
    }

    override fun onError(error: Int) {
    }

    override fun onResults(results: Bundle?) {
       val micButton = findViewById<ImageButton>(R.id.button_spell_item_IB)/**if(articleViewModel?.viewArticleType == ArticleViewType.NAME){R.id.button_spell_article_iv}else{R.id.button_spell_description_iv})*/
        micButton?.setImageResource(R.drawable.ic_baseline_mic_24)
        val data: ArrayList<String> =
            results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION) as ArrayList<String>
        val inputET = findViewById<EditText>(R.id.text_spelled_article_et)/**if(articleViewModel?.viewArticleType == ArticleViewType.NAME){R.id.text_spelled_article_et}else{R.id.text_spelled_description_et})*/
        inputET?.setText(data[0])
//        if(articleViewModel?.typeItemView == ArticleAddViewModel.InputType.ITEM_NAME){
//            articleViewModel?.itemModel?.nameItem = inputET.text.toString()
//            articleViewModel?.typeItemView = ArticleAddViewModel.InputType.ITEM_DESCRIPTION
//        }else{
//            articleViewModel?.itemModel?.descriptionItem = inputET.text.toString()
//            articleViewModel?.typeItemView = ArticleAddViewModel.InputType.ITEM_CODE
//        }
//        articleViewModel?.animateViewType()
//
//        inputET.setText("")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
         //data?.extras?.get("data") as Bitmap
            when (requestCode) {
                BaseConstants.PERMISSION_CROP_IMAGE -> {
                    try {
                        val imageUri: Uri? = data?.data
                        val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
                        val imageEncoded = bitmap.encodeToString()
                        //   showMessage("Image Size${imageBitmap.getBitmapSize()} // ${imageBitmap.getBitmapSize()/1000000}")
                        articleViewModel?.itemModel?.imageItem = imageEncoded
                        articleViewModel?.typeItemView = ArticleAddViewModel.InputType.ITEM_COMPLETE
                        articleViewModel?.animateViewType()
                    } catch (e: java.lang.Exception) {
                        Log.e("ENCODE IMAGE", "${e.message}")
                    }
                }
                BaseConstants.IMAGE_CAPTURE_CODE -> {
                    if (resultCode == -1) {
                        val imageBitmap = MediaStore.Images.Media.getBitmap(
                            contentResolver, imageUri
                                ?: return
                        )
//                Preferences.saveDataPreference(this,
//                        PreferenceTypeKey.PROFILE_PICTURE, imageBitmap.encodeToString() ?: return)
                        try {
                            val imageEncoded = imageBitmap.encodeToString()
                         //   showMessage("Image Size${imageBitmap.getBitmapSize()} // ${imageBitmap.getBitmapSize()/1000000}")
                             articleViewModel?.itemModel?.imageItem = imageEncoded
                             articleViewModel?.typeItemView = ArticleAddViewModel.InputType.ITEM_COMPLETE
                             articleViewModel?.animateViewType()

                        } catch (e: Exception) {
                            Log.e("ENCODE IMAGE", "${e.message}")
                        }
                    }
                }
                BaseConstants.SERVICO_DETALHES_REQUEST -> {
                    try {
                        val idCode = data?.getLongExtra(BaseConstants.IntentKeys.IS_LOGIN_VIEW.name, 0)
                        //showMessage("$idCode")
                        findViewById<EditText>(R.id.text_spelled_article_et).setText("${idCode ?: "SIN CODIGO VALIDO"}")

                     /**  articleViewModel?.updateCompleteItems()*/
                    } catch (e: Exception){
                        Log.e("ID CODE", "${e.message}")
                    }
                }
                BaseConstants.PICK_IMAGE_FROM_GALERY -> {
                    val intent =  Intent(Intent.ACTION_EDIT)//Intent("com.android.camera.action.CROP")
                    // intent.setClassName("com.android.camera", "com.android.camera.CropImage")
                    val uri: Uri? = data?.data
                    intent.data = (uri)
                    val cropRequest = uri?.let { CropRequest.Auto(sourceUri = it, requestCode = BaseConstants.PERMISSION_CROP_IMAGE ) }
                    cropRequest?.let { Croppy.start(this, it) }

                }

            }

    }

    override fun onPlaceChangeHandler(place: AddressByLocationModel?) {
        val LOC = GpsTracker(this).locationGps
        articleViewModel?.itemModel?.latitude =  "${LOC?.latitude}"
        articleViewModel?.itemModel?.longitude = "${LOC?.longitude}"
        articleViewModel?.itemModel?.located = "${place?.locality ?: ""}, ${place?.localityInfo?.administrative?.last()?.description ?: ""}"
        (findViewById(R.id.see_at_article_TV) as? TextView)?.text  =   articleViewModel?.itemModel?.located
    }
}