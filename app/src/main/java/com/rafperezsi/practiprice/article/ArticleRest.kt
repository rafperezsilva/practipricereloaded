package com.rafperezsi.practiprice.article

import android.location.Location
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.rafperezsi.practiprice.R
import com.rafperezsi.practiprice.utils.BaseActivity
import com.rafperezsi.practiprice.utils.BaseLoader
import org.json.JSONObject
import kotlin.properties.Delegates


class ArticleRest (private val context: BaseActivity){
    init {

    }

    fun addNewArticle(article:ArticleItem, observer: ArticleObserver){
        val mQueue = Volley.newRequestQueue(context)
       BaseLoader(context).showLoader()
        val jsonObject = JSONObject(article.toJson())
        val jsonObjectRequest = object : JsonObjectRequest(
                Method.POST,
                context.getString(R.string.url_base) + "user/article_item",
                jsonObject,
                Response.Listener<JSONObject> { response ->
                    BaseLoader(context).hideLoader()
                    Log.d("ADD ARTICLE", response.toString())
                },
            Response.ErrorListener { error ->
                BaseLoader(context).hideLoader()
                //                        if (error.networkResponse == null) {
                //                            return
                //                        }
                Log.e("TAG", error.message, error)
            }) { //no semicolon or coma
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
                Log.d("ON ARTICLE ADDDED",response.toString())
                BaseLoader(context).hideLoader()
                    when(response?.statusCode){
                        200 -> {
                            observer.articleAdded = true
                        }
                        else -> {
                            observer.articleAdded = false
                        }
                    }
                return super.parseNetworkResponse(response)
            }
        }
        Log.i("LOGIN_REQUEST","URL:\n ${jsonObjectRequest.url} \nHEADERS:\n ${jsonObjectRequest.headers}\nBODY:\n ${jsonObjectRequest.body}")
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy().customPolicy()

        mQueue.add(jsonObjectRequest)
    }

    fun getArticleList(observer: ArticleObserver){
        val mQueue = Volley.newRequestQueue(context)
        BaseLoader(context).showLoader()
       // val jsonObject = JSONObject(article.toJson())
        val jsonObjectRequest = object : JsonObjectRequest(
                Method.GET,
                context.getString(R.string.url_base) + "user/articles",
                null,
                Response.Listener<JSONObject> { response ->
                    BaseLoader(context).hideLoader()
                    Log.d("LOGIN RESPONSE", response.toString())
                    try {
                        observer.articleList =  ArticleListModel.fromJson(response.toString())
                    } catch (e: Exception) {
                        println(e)
                        //baseLoader?.hideLoader()
                    }
                    Log.d("ADD ARTICLE", response.toString())
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(error: VolleyError) {
                        Log.e("TAG", error.message, error)
                        BaseLoader(context).hideLoader()
                        if (error.networkResponse == null) {
                            return
                        }

                    }
                }) { //no semicolon or coma
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
                Log.d("ON ARTICLE ADDDED",response.toString())
                    when(response?.statusCode){
                        200 -> {

                        }
                        else -> {

                        }
                    }
                return super.parseNetworkResponse(response)
            }
        }
        Log.d("LOGIN_REQUEST","URL:\n ${jsonObjectRequest.url} \nHEADERS:\n ${jsonObjectRequest.headers}\nBODY:\n ${jsonObjectRequest.body}")
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy().customPolicy()
        mQueue.add(jsonObjectRequest)
    }

    fun getAddressByLocation(location: Location, observer: PlaceDirectionsObserver){
        val mQueue = Volley.newRequestQueue(context)
        BaseLoader(context).showLoader()
       // val jsonObject = JSONObject(article.toJson())
        val jsonObjectRequest = object : JsonObjectRequest(
                Method.GET,
                context.getString(R.string.url_geocoding) + "latitude=${location.latitude}&longitude=${location.longitude}&localityLanguage=es",
                null,
                Response.Listener<JSONObject> { response ->
                    BaseLoader(context).hideLoader()
                    Log.d("LOGIN RESPONSE", response.toString())
                    try {
                        observer.placeLisener =  AddressByLocationModel.fromJson(response.toString())
                    } catch (e: Exception) {
                        println(e)
                        //baseLoader?.hideLoader()
                    }
                    Log.d("ADD ARTICLE", response.toString())
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(error: VolleyError) {
                        Log.e("TAG", error.message, error)
                        BaseLoader(context).hideLoader()
                        if (error.networkResponse == null) {
                            return
                        }

                    }
                }) { //no semicolon or coma
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
                Log.d("ON ARTICLE ADDDED",response.toString())
                    when(response?.statusCode){
                        200 -> {

                        }
                        else -> {

                        }
                    }
                return super.parseNetworkResponse(response)
            }
        }
        Log.d("LOGIN_REQUEST","URL:\n ${jsonObjectRequest.url} \nHEADERS:\n ${jsonObjectRequest.headers}\nBODY:\n ${jsonObjectRequest.body}")
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy().customPolicy()
        mQueue.add(jsonObjectRequest)
    }
}


interface ArticleDelegate {
    fun onArticleAdded(isAdded: Boolean)
    fun onArticlesRetrieved(articles: ArticleListModel)
}

class ArticleObserver(articleLisener:  ArticleDelegate) {
    var articleAdded:  Boolean by Delegates.observable(
            initialValue =  false,
            onChange = { _, _, new -> articleLisener.onArticleAdded(new) })
    var articleList:  ArticleListModel by Delegates.observable(
            initialValue =  ArticleListModel(),
            onChange = { _, _, new -> articleLisener.onArticlesRetrieved(new) })

}

fun DefaultRetryPolicy.customPolicy(): DefaultRetryPolicy {
    return DefaultRetryPolicy(
        150000,
        0,
        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
    )

}


interface PlaceDirectionsDelegates {
    fun onPlaceChangeHandler(place: AddressByLocationModel?)

}


class PlaceDirectionsObserver(lisener: PlaceDirectionsDelegates) {
    //val location: Location? = null
    var placeLisener: AddressByLocationModel by Delegates.observable(
        initialValue = AddressByLocationModel(),
        onChange = { prop, old, new -> lisener.onPlaceChangeHandler(new) })
}