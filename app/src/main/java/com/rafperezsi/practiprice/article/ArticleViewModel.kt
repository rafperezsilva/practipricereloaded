package com.rafperezsi.practiprice.article

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Looper
import android.provider.MediaStore
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.widget.addTextChangedListener
import com.bluecoltan.dottys.utils.extensions.encodeToBitmap
import com.bluecoltan.dottys.utils.extensions.roundOffDecimal
import com.rafperezsi.practiprice.CameraScannerActivity
import com.rafperezsi.practiprice.R
import com.rafperezsi.practiprice.utils.BaseConstants
import com.rafperezsi.practiprice.utils.BaseConstants.RECORD_AUDIO_REQUEST_CODE
import com.rafperezsi.practiprice.utils.requestCameraPermission
import java.lang.Exception
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import kotlin.reflect.full.memberProperties

enum class  ArticleViewType { NAME, DESCRIPTION, COMPLETE}

class ArticleViewModel(private val context: ArticleActivity) {
    private lateinit var speechRecognizer: SpeechRecognizer


    @SuppressLint("ClickableViewAccessibility")
    fun initSpellRecognition() {

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context)
        val speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        speechRecognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        speechRecognizer.setRecognitionListener(context)
        val micButton = context.findViewById<ImageButton>(R.id.button_spell_item_IB)//if(viewArticleType == ArticleViewType.NAME){R.id.button_spell_article_iv}else{R.id.button_spell_description_iv})  as? ImageView


        micButton?.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                speechRecognizer.stopListening()
                micButton.setImageResource(R.drawable.ic_baseline_mic_24)
                micButton.alpha  = 1f
                micButton.scaleX = 1f
                micButton.scaleY = 1f
            }
            if (event.action == MotionEvent.ACTION_DOWN) {
                try {
                    micButton.setImageResource(R.drawable.ic_baseline_mic_none_24)
                    micButton.alpha  = 0.5f
                    micButton.scaleX = 1.2f
                    micButton.scaleY = 1.2f
                    speechRecognizer.startListening(speechRecognizerIntent)
                } catch (e: Exception) {
                    context.showMessage("${e.message}")
                }
            }
            true
        }

    }


/*
    :  View.OnClickListener,
    ArticleDelegate, View.OnFocusChangeListener, View.OnKeyListener, TextWatcher {
    var viewArticleType = ArticleViewType.NAME
    val RECORD_AUDIO_REQUEST_CODE = 1
    private var speechRecognizer: SpeechRecognizer? = null
    var itemModel = ArticleItem()
    var imageUri: Uri? = null

    init {
        checkPermission()
        initArticleInputs()
        updateCompleteItems()
    }

    private fun initArticleInputs(){
        (context.findViewById(R.id.done_text_submit_article_iv) as? ImageView)?.setOnClickListener(this)
        (context.findViewById(R.id.done_text_submit_description_iv) as? ImageView)?.setOnClickListener(this)
        (context.findViewById(R.id.code_scan_image_button) as? ImageView)?.setOnClickListener(this)
        (context.findViewById(R.id.take_image_image_button) as? ImageView)?.setOnClickListener(this)
        (context.findViewById(R.id.price_item_et) as? EditText)?.setOnFocusChangeListener(this)
        (context.findViewById(R.id.price_item_et) as? EditText)?.addTextChangedListener (this)
        (context.findViewById(R.id.price_item_et) as? EditText)?.setOnKeyListener(this)
        (context.findViewById(R.id.disscount_item_et) as? EditText)?.setOnFocusChangeListener(this)
        (context.findViewById(R.id.disscount_item_et) as? EditText)?.addTextChangedListener(this)
        (context.findViewById(R.id.disscount_item_et) as? EditText)?.setOnKeyListener(this)
       // (context.findViewById(R.id.item_article_cardview) as? ConstraintLayout)?.visibility = View.GONE
        animateInputsLayouts()
    }

    override fun onClick(v: View?) {
        val animation = AnimationUtils.loadAnimation(context,R.anim.bounce_in)
        (context.findViewById(v?.id ?: 0) as? ImageButton)?.startAnimation(animation)
        when(v?.id){
            R.id.done_text_submit_article_iv -> {
                context.hideKeyboard()
                val nameET = (context.findViewById(R.id.text_spelled_article_et) as? EditText)
                if(!nameET?.text?.toString().isNullOrEmpty()){
                    viewArticleType = ArticleViewType.DESCRIPTION
                    itemModel.nameItem = nameET?.text?.toString()
                    animateInputsLayouts()
                    updateCompleteItems()
                    nameET?.setText("")
                } else {
                    context.showMessage("Nombre del artículo requerido")
                }
            }
            R.id.done_text_submit_description_iv -> {
                context.hideKeyboard()
                val descriptionET = (context.findViewById(R.id.text_spelled_description_et) as? EditText)
                if(!descriptionET?.text?.toString().isNullOrEmpty()){
                    viewArticleType = ArticleViewType.COMPLETE
                    itemModel.descriptionItem = descriptionET?.text?.toString()
                    updateCompleteItems()
                    descriptionET?.setText("")
                } else {
                    context.showMessage("Descripción  del artículo requerido")
                }
            }
            R.id.code_scan_image_button -> {
                if (context.requestCameraPermission(context, BaseConstants.PERMISSION_CODE_CAMERA))
                    context.startActivityForResult(
                        Intent(
                            context,
                            CameraScannerActivity::class.java
                        ), BaseConstants.SERVICO_DETALHES_REQUEST
                    )
            }
            R.id.take_image_image_button -> {
                if (context.requestCameraPermission(
                        context,
                        BaseConstants.PERMISSION_CAPTURE_CAMERA
                    )
                )
                    dispatchTakePictureIntent()
            }
             R.id.name_article -> {
                 if(!context.findViewById<TextView>(R.id.name_article).text.isNullOrBlank() &&
                         viewArticleType != ArticleViewType.NAME) {
                     viewArticleType = ArticleViewType.NAME
                     itemModel.nameItem = ""
                     animateInputsLayouts()
                     updateCompleteItems()
                 }
             }
             R.id.description_article -> {
                 if(!context.findViewById<TextView>(R.id.description_article).text.isNullOrBlank() &&
                     viewArticleType == ArticleViewType.COMPLETE) {
                     viewArticleType = ArticleViewType.DESCRIPTION
                     itemModel.descriptionItem = ""
                     animateInputsLayouts()
                     updateCompleteItems()
                 }
             }
            R.id.add_new_article_button -> {
                (context.findViewById(R.id.add_new_article_button) as? Button)?.alpha = 0.5f
             //   (context.findViewById(R.id.add_new_article_button) as? Button)?.isEnabled = false
                ArticleRest(context).addNewArticle(itemModel, ArticleObserver(this))
            }
        }

    }

    fun updateCompleteItems(){
          itemModel.imageItem?.let { (context.findViewById(R.id.article_image_view) as? ImageView)?.setImageBitmap(it.encodeToBitmap())}
          (context.findViewById(R.id.name_article) as? TextView)?.text = itemModel.nameItem
          (context.findViewById(R.id.description_article) as? TextView)?.text =  itemModel.descriptionItem
          (context.findViewById(R.id.name_article) as? TextView)?.setOnClickListener(this)
          (context.findViewById(R.id.description_article) as? TextView)?.setOnClickListener(this)
          (context.findViewById(R.id.code_article) as? TextView)?.text = "${itemModel.codeItem ?: ""}"
          (context.findViewById(R.id.disscount_item_et) as? EditText)?.text?.let {
              if(!it.toString().isNullOrEmpty())
                  try {

              itemModel.discountItem = it.toString().toDouble().roundOffDecimal()

                  }catch (e: Exception){
                      Log.e("DISSCOUNT ERROR", "${e.message}")
                  }
          }
          (context.findViewById(R.id.price_item_et) as? EditText)?.text?.let{
              if(!it.toString().isNullOrEmpty())
                  try {
                      itemModel.priceItem = it.toString().toDouble().roundOffDecimal()
                  }  catch (e: Exception){
                      Log.e("PRICE ERROR", "${e.message}")
                  }
          }
          (context.findViewById(R.id.add_new_article_button) as? Button)?.visibility = if(checkCompleteValues()){View.VISIBLE}else{View.GONE}
//          (context.findViewById(R.id.add_new_article_button) as? Button)?.visibility = if(checkCompleteValues() && !itemModel.codeItem.isNullOrEmpty()){View.VISIBLE}else{View.GONE}
          (context.findViewById(R.id.add_new_article_button) as? Button)?.setOnClickListener(this)

      }

    private fun checkCompleteValues(): Boolean{
        for (prop in ArticleItem::class.memberProperties) {
            if(prop.get(itemModel) == null) {
               /* when(prop.name) {
                    "discount" -> {}
                    "idCode"  -> {}
                    "imageBase"  -> {}
                    "imagenUrl" -> {}
                    "itemDescription"  -> {}
                    "itemName"  -> {}
                    "price"  -> {}
                }*/
                if(prop.name != "idItem" && prop.name != "priceDoubleItem" &&
                    prop.name != "discountDoubleItem" &&
                    prop.name != "discountItem"  &&
                    prop.name != "quantity" )
                return false
            } else {
                (context.findViewById(R.id.item_article_cardview) as? ConstraintLayout)?.visibility = View.VISIBLE
            }
        }
        if(itemModel.codeItem.isNullOrEmpty()){
            return false
        }
        return true
    }

    private fun animateInputsLayouts(){
      val inputDescriptionLayout =  (context.findViewById(R.id.article_description_container_layout) as? ConstraintLayout)
      val inputNameLayout =  (context.findViewById(R.id.article_name_container_layout) as? ConstraintLayout)
       when (viewArticleType) {
           ArticleViewType.NAME -> {
               inputDescriptionLayout?.animate()?.scaleY(0f)?.setDuration(0)?.start()
               inputDescriptionLayout?.visibility = View.GONE
               if(inputNameLayout?.visibility == View.GONE)
               inputNameLayout?.visibility = View.VISIBLE
               inputNameLayout?.animate()?.scaleY(1f)?.setDuration(250)?.start()
           }
           ArticleViewType.DESCRIPTION -> {
               inputNameLayout?.animate()?.scaleY(0f)?.setDuration(250)?.withEndAction {
                   inputNameLayout?.visibility = View.GONE
                   inputDescriptionLayout?.visibility = View.VISIBLE
                   inputDescriptionLayout?.animate()?.scaleY(1f)?.setDuration(250)?.start()
               }
           }
           else -> {}
       }
        initSpellRecognition()
    }




    internal fun dispatchTakePictureIntent() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        imageUri = context.contentResolver?.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // cameraIntent.putExtra("android.intent.extra.", true);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        context.startActivityForResult(cameraIntent, BaseConstants.IMAGE_CAPTURE_CODE)
    }

    override fun onArticleAdded(isAdded: Boolean) {
        val msg = if(isAdded) {
            "El artículo ha sido agregado satisfactoriamente"
        } else {
            "Hubo un problema al ingresar el artículo, intente de nuevo por favor"
        }

        android.os.Handler(Looper.getMainLooper()).post {
            context.showMessage(msg)
            (context.findViewById(R.id.add_new_article_button) as? Button)?.alpha = 1f
            (context.findViewById(R.id.add_new_article_button) as? Button)?.isEnabled = true
            android.os.Handler().postDelayed({
                if (isAdded)
                    context.finish()
            }, 2000)
        }
    }

    override fun onArticlesRetrieved(articles: ArticleListModel) {
    }

//    override fun afterTextChanged(s: Editable?) {
//
//    }
//
//    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//    }
//
//    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//    }


    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        updateCompleteItems()
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER && event?.action == KeyEvent.ACTION_UP) {
            updateCompleteItems()
            context.hideKeyboard()
            return true
        }
        return false
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
     }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
     }

    override fun afterTextChanged(s: Editable?) {
        updateCompleteItems()
     }*/
}